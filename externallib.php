<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Self enrol plugin external functions
 *
 * @package    enrol_self
 * @copyright  2013 Rajesh Taneja <rajesh@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once("$CFG->libdir/externallib.php");

/**
 * Self enrolment external functions.
 *
 * @package   enrol_self
 * @copyright 2012 Rajesh Taneja <rajesh@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since     Moodle 2.6
 */
class local_api_external extends external_api
{

    /**
     * Returns description of get_instance_info() parameters.
     *
     * @return external_function_parameters
     */
    public static function get_courses_info_parameters()
    {
        return new external_function_parameters(
            array()
        );
    }

    public static function set_enrol_user_parameters()
    {
        return new external_function_parameters(
            array('enrols' => new external_multiple_structure(
                new external_single_structure(array(
                        'course' => new external_value(PARAM_TEXT, 'id of course in 1C', false, ''),
                        'test' => new external_value(PARAM_TEXT, 'id of test in 1C', false, ''),
                        'date' => new external_value(PARAM_INT, 'date to UNIXTIME', false, 0),
                        'users' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'username'), 'users', false, array()
                        )

                    )
                )
            )
            )
        );
    }

    public static function set_courses_info_parameters()
    {
        return new external_function_parameters(
            array('courses' => new external_multiple_structure(
                new external_single_structure(array(
                        'id' => new external_value(PARAM_INT, 'id of course/category in Moodle', false, 0),
                        'idNumber' => new external_value(PARAM_TEXT, 'id of course/category in 1C', true, ''),
                        'name' => new external_value(PARAM_TEXT, 'course/category name', false, ''),
                        'parentCategory' => new external_value(PARAM_TEXT, 'name of enrolment plugin', false, 0),
                        'isCategory' => new external_value(PARAM_BOOL, 'if category when true', false, false),
                        'users' => new external_multiple_structure(
                            new external_value(PARAM_TEXT, 'username'), 'users', false, array()
                        )

                    )
                )
            )
            )
        );
    }

    public static function get_grades_parameters()
    {
        return new external_function_parameters(
            array('tests' => new external_multiple_structure(
                new external_single_structure(array(
                        'test' => new external_value(PARAM_TEXT, 'id of test in 1C', false, ''),
                    )
                )
            )
            )
        );
    }

    public static function enrol_to_course($username, $courseid, $role = 4)
    {
        global $DB;
        $user = $DB->get_record('user', array('username' => $username));
        if (empty($user)) $user = $DB->get_record('user', array('idnumber' => $username));
        if (empty($user)) return false;
        $enrol = $DB->get_record('enrol', array('enrol' => 'manual', 'courseid' => $courseid));
        if (empty($enrol)) return false;
        $enrol_robokassa = enrol_get_plugin('manual');
        $enrol_robokassa->enrol_user($enrol, $user->id, $role, time(), 0);
        return true;

    }

    public static function get_contacts_by_courses($course)
    {
        global $CFG, $DB;
        $mas = array();
        require_once($CFG->libdir . '/coursecatlib.php');
        $course = new course_in_list($course);
        if ($course->has_course_contacts()) {
            foreach ($course->get_course_contacts() as $userid => $coursecontact) {
                $u = $DB->get_record('user', array('id' => $userid));
                $mas[] = $u;
            }

        }
        return $mas;
    }

    public static function get_user_by_courses($courseid, $roleid)
    {
        global $DB;
        $context = context_course::instance($courseid);
        list($gradebookroles_sql, $params) = $DB->get_in_or_equal($roleid, SQL_PARAMS_NAMED, 'grbr');
        list($enrolledsql, $enrolledparams) = get_enrolled_sql($context);
        $relatedcontexts = get_related_contexts_string($context);
        $params = array_merge($params, $enrolledparams);
        $users_sql = "SELECT u.id, u.username
                        FROM {user} u
                        JOIN ($enrolledsql) je ON je.id = u.id

                        JOIN (
                                  SELECT DISTINCT ra.userid
                                    FROM {role_assignments} ra
                                   WHERE ra.roleid $gradebookroles_sql
                                     AND ra.contextid $relatedcontexts
                             ) rainner ON rainner.userid = u.id
                         WHERE u.deleted = 0

                    ORDER BY firstname";
        return $DB->get_records_sql($users_sql, $params);
    }

    public static function set_enrol_user($enrols)
    {
        global $DB;
        $answers = array();
        $mas_enrols = array();
        foreach ($enrols as $enrol) {
            $answersForUsers = array();
            $answerForCourse = 'OK';
            $course = null;
            if ($enrol['course'])
                $course = $DB->get_record_sql("select * from {course}
                 where idnumber = '{$enrol['course']}' or id = '{$enrol['course']}'");
            if (empty($course) && ($enrol['test']))
                $course = $DB->get_record_sql("select c.* from {course_modules} cm, {course} c
                 where cm.idnumber = '{$enrol['test']}' and cm.course = c.id");

            if (empty($course)) {
                $answerForCourse = 'Курс ' . $enrol['course'] . " не найден";
                $answers[] = array('answer' => $answerForCourse, 'users' => $answersForUsers);
                continue;
            }
            /*if (empty($enrol['users'])) {
                $answerForCourse = "Пользователи не переданы";
                $answers[] = array('answer' => $answerForCourse, 'users' => $answersForUsers);
                continue;
            }*/
            if (!isset($mas_enrols[$course->id])) $mas_enrols[$course->id] = array();
            if (empty($enrol['users']))
                foreach ($enrol['users'] as $username) {
                    $ob = new stdClass();
                    $ob->courseid = $course->id;
                    $ob->timestart = $enrol['date'];
                    $ob->timeinsert = time();
                    $ob->username = $username;
                    //$DB->insert_record("api_enrols", $ob);
                    $mas_enrols[$course->id][] = $ob;
                    $answersForUsers[] = "OK";

                }
            if($enrol['test'] == 'Test') $mas_enrols[$course->id] = array();
            $answers[] = array('answer' => $answerForCourse, 'users' => $answersForUsers);
        }

        foreach($mas_enrols as $courseid => $obs)
        {
            $DB->delete_records('api_enrols', array('courseid' => $courseid));
            $DB->delete_records('api_enrols_db', array('courseid' => $courseid));
            if($obs)
            {
                foreach($obs as $ob)
                {
                    $DB->insert_record("api_enrols", $ob);
                    $DB->insert_record("api_enrols_db", $ob);
                }
            }
        }

        return $answers;
    }

    public static function get_grades_by_test($courseid)
    {
        global $DB;
        $sql = "select a.userid, a.sumgrades, a.timefinish from
                mdl_quiz as q, mdl_quiz_attempts as a
                where
                q.course = $courseid and
                a.quiz = q.id and
                a.timefinish > 0 group by a.userid order by a.timefinish DESC";
        return $DB->get_records_sql($sql);
    }

    public static function get_grades($tests)
    {
        global $DB;
        $answer = array();
        foreach ($tests as $ob) {
            if ($ob['test'])
                $course = $DB->get_record_sql("select * from {course}
                 where idnumber = '{$ob['test']}' or id = '{$ob['test']}'");
            if (empty($course) && ($ob['test']))
                $course = $DB->get_record_sql("select c.* from {course_modules} cm, {course} c
                 where cm.idnumber = '{$ob['test']}' and cm.course = c.id");
            if ($course) {
                if ($users = self::get_user_by_courses($course->id, 5)) {
                    $grades = self::get_grades_by_test($course->id);
                    foreach ($users as $user) {
                        $ob = new stdClass();
                        $ob->id = $user->username;
                        $ob->grade = isset($grades[$user->id]) ? $grades[$user->id]->sumgrades : 0;
                        $ob->idNumber = $ob['test'];
                        $ob->date = isset($grades[$user->id]) ? $grades[$user->id]->timefinish : 0;
                        $answer[] = $ob;
                    }
                }

            }
        }
        return $answer;
    }

    public static function set_courses_info($courses)
    {
        global $DB, $CFG;
        require_once($CFG->dirroot . "/course/lib.php");
        require_once($CFG->libdir . '/completionlib.php');
        require_once($CFG->libdir . "/coursecatlib.php");
        ob_start();
        echo count($courses);
        print_r($courses);
        file_put_contents($CFG->tempdir . '/courses.txt', ob_get_clean());
        $num = 0;
        $log = '';
        $ikey = -1;

        $delete_course_keys = array();
        $delete_course = array();
        $delete_category_keys = array();
        $delete_category = array();

        foreach ($courses as $course) {
            $ikey++;

            $log .= "\n$num";
            $users = $course['users'];
            $answer = 'OK';
            $newcourse = !$course['isCategory'] && !$DB->record_exists('course', array('id' => $course['id'])) &&
                !$DB->record_exists('course', array('idnumber' => $course['idNumber'])) &&
                $course['idNumber'];

            $newcategory = $course['isCategory'] && !$DB->record_exists('course_categories', array('id' => $course['id'])) &&
                !$DB->record_exists('course_categories', array('idnumber' => $course['idNumber'])) &&
                $course['idNumber'];
            $new = $newcourse || $newcategory;
            if (empty($course['name'])) {
                $mas[] = '';
                if ($course['isCategory']) {
                    $delete_category[$ikey] = $course;
                    $delete_category_keys[] = $ikey;
                } else {
                    $delete_course[$ikey] = $course;
                    $delete_course_keys[] = $ikey;
                }
                continue;
            }

            if ($new) {

                if (empty($course['name'])) {
                    $answer = 'Нет названия';
                    $mas[] = array('answer' => $answer);
                    continue;
                }
                //throw new moodle_exception('noemptycoursename', 'local_api', '', $course['name']);
                if (!$course['isCategory']) {
                    //if ($DB->get_record('course', array('fullname' => $course['name']))) {$answer = 'Курс с таким названием есть';$mas[] = array('answer' => $answer);continue;}
                    //throw new moodle_exception('hascourse', 'local_api', '', $course['name']);

                    //if ($DB->get_record('course', array('shortname' => $course['name']))) {$answer = 'Курс с таким коротким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                    //throw new moodle_exception('hascourse', 'local_api', '', $course['name']);
                    if ($course['idNumber'])
                        if ($DB->get_record('course', array('idnumber' => $course['idNumber']))) {
                            $answer = 'Курс с таким idNumber уже есть';
                            $mas[] = array('answer' => $answer);
                            continue;
                        }
                    //throw new moodle_exception('hascourse', 'local_api', '', $course['idNumber']);
                    $course['category'] = 1;
                    if ($course['parentCategory'])
                        if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'")) {
                            $course['category'] = $category->id;
                        } else {
                            $answer = 'Не найдена категория курса по idNumber';
                            $mas[] = array('answer' => $answer);
                            continue;
                        };
                    //throw new moodle_exception('nohascategories', 'local_api', '', $course['parentCategory']);
                    $course['fullname'] = $course['name'];
                    //$course['shortname'] = $course['name'];
                    $pre = '';
                    $i = 0;
                    while ($DB->record_exists('course', array('shortname' => $course['name'] . $pre))) {
                        $i++;
                        $pre = "_" . $i;
                    }
                    $course['shortname'] = $course['name'] . $pre;
                    $course['idnumber'] = $course['idNumber'];

                    try {
                        $course = create_course((object)$course);
                    } catch (Exception $ex) {
                        $answer = $ex->getMessage();
                        $mas[] = array('answer' => $answer);
                        continue;
                    }
                } else {
                    $course['parent'] = 0;

                    if ($course['parentCategory'])
                        if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'")) {
                            //throw new moodle_exception('unknowcategory');
                            $course['parent'] = $category->id;
                        }


                    //if ($DB->get_record('course_categories', array('name' => $course['name']))) {$answer = 'Категория с таким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                    //throw new moodle_exception('hascourse', 'local_api', '', $course['name']);
                    //$course['parent'] = $course['parentCategory'];
                    $course['idnumber'] = $course['idNumber'];
                    try {
                        $course['id'] = coursecat::create((object)$course)->id;
                    } catch (Exception $ex) {
                        $answer = $ex->getMessage();
                        $mas[] = array('answer' => $answer);
                        continue;
                    }

                }
            } elseif ($course['id']) {
                if (!$course['isCategory']) {
                    if ($course['id']) {
                        //if ($DB->get_record_sql('select id from {course} where id <> '.$course['id'].' and fullname = \''.$course['name'].'\'')) {$answer = 'Курс с таким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                        //if ($DB->get_record_sql('select id from {course} where id <> '.$course['id'].' and shortname = \''.$course['name'].'\'')) {$answer = 'Курс с таким коротким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                        if ($courseob = $DB->get_record('course', array('id' => $course['id']))) {
                            $courseob->fullname = $course['name'];
                            //$courseob->shortname = $course['name'];
                            $pre = '';
                            $i = 0;
                            while ($DB->record_exists('course', array('shortname' => $course['name'] . $pre))) {
                                $i++;
                                $pre = "_" . $i;
                            }
                            $courseob->shortname = $course['name'] . $pre;
                            if ($course['parentCategory']) {
                                if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'")) {
                                    $courseob->category = $category->id;

                                } else {
                                    $answer = 'Не найдена категория с таким idNumber';
                                    $mas[] = array('answer' => $answer);
                                    continue;
                                }
                            }
                            if ($course['idNumber']) {
                                if ($DB->get_record_sql('select id from {course} where id <> ' . $course['id'] . ' and idnumber = \'' . $course['idNumber'] . '\'')) {
                                    $answer = 'Курс с таким idNumber уже есть';
                                    $mas[] = array('answer' => $answer);
                                    continue;
                                };
                                $courseob->idnumber = $course['idNumber'];
                            }
                            try {
                                update_course($courseob);
                            } catch (Exception $ex) {
                                $answer = $ex->getMessage();
                                $mas[] = array('answer' => $answer);
                                continue;
                            }
                        } else {
                            $answer = 'Курс с таким id не найден';
                            $mas[] = array('answer' => $answer);
                            continue;
                        }
                    }
                } else {

                    //if ($DB->get_record_sql('select id from {course_categories} where id <> '.$course['id'].' and name = \''.$course['name'].'\'')) {$answer = 'Категория с таким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                    if ($courseob = $DB->get_record('course_categories', array('id' => $course['id']))) {
                        $courseob->name = $course['name'];
                        if ($course['parentCategory']) {
                            if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'"))
                                $courseob->parent = $category->id;
                            else {
                                $answer = 'Не найдена категория с таким idNumber';
                                $mas[] = array('answer' => $answer);
                                continue;
                            }
                        }
                        if ($course['idNumber']) {
                            if ($DB->get_record_sql('select id from {course_categories} where id <> ' . $course['id'] . ' and idnumber = \'' . $course['idNumber'] . '\'')) {
                                $answer = 'Категория с таким idNumber уже есть';
                                $mas[] = array('answer' => $answer);
                                continue;
                            }
                            $courseob->idnumber = $course['idNumber'];
                        }
                        try {
                            $category = coursecat::get($courseob->id);
                            $category->update($courseob);
                        } catch (Exception $ex) {
                            $answer = $ex->getMessage();
                            $mas[] = array('answer' => $answer);
                            continue;
                        }
                    } else {
                        $answer = 'Категория с таким id не найдена';
                        $mas[] = array('answer' => $answer);
                        continue;
                    }
                }
            } elseif ($course['idNumber']) {
                if (!$course['isCategory']) {
                    if ($course['idNumber']) {
                        //if ($DB->get_record_sql('select id from {course} where idnumber <> \''.$course['idNumber'].'\' and fullname = \''.$course['name'].'\'')) {$answer = 'Курс с таким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                        //if ($DB->get_record_sql('select id from {course} where idnumber <> \''.$course['idNumber'].'\' and shortname = \''.$course['name'].'\'')) {$answer = 'Курс с таким коротким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                        if ($courseob = $DB->get_record('course', array('idnumber' => $course['idNumber']))) {
                            $course['id'] = $courseob->id;
                            $courseob->fullname = $course['name'];
                            //$courseob->shortname = $course['name'];
                            $pre = '';
                            $i = 0;
                            while ($DB->record_exists('course', array('shortname' => $course['name'] . $pre))) {
                                $i++;
                                $pre = "_" . $i;
                            }
                            $courseob->shortname = $course['name'] . $pre;
                            if ($course['parentCategory']) {
                                if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'"))
                                    $courseob->category = $category->id;
                                else {
                                    $answer = 'Категория с таким idNumber не найдена';
                                    $mas[] = array('answer' => $answer);
                                    continue;
                                }
                            }
                            try {
                                update_course($courseob);
                            } catch (Exception $ex) {
                                $answer = $ex->getMessage();
                                $mas[] = array('answer' => $answer);
                                continue;
                            }

                        } else {
                            $answer = 'Курс с таким idNumber не найден';
                            $mas[] = array('answer' => $answer);
                            continue;
                        }
                    }
                } else {

                    //if ($DB->get_record_sql('select id from {course_categories} where idnumber <> \''.$course['idNumber'].'\' and name = \''.$course['name'].'\'')) {$answer = 'Категория с таким названием уже есть';$mas[] = array('answer' => $answer);continue;}
                    if ($courseob = $DB->get_record('course_categories', array('idnumber' => $course['idNumber']))) {

                        $courseob->name = $course['name'];
                        if ($course['parentCategory']) {
                            if ($category = $DB->get_record_sql("select * from {course_categories} where idnumber = '{$course['parentCategory']}' or id = '{$course['parentCategory']}'"))
                                $courseob->parent = $category->id;
                            else {
                                $answer = 'Категория с таким idNumber не найдена';
                                $mas[] = array('answer' => $answer);
                                continue;
                            }
                        }
                        try {
                            $category = coursecat::get($courseob->id);
                            $category->update($courseob);
                        } catch (Exception $ex) {
                            $answer = $ex->getMessage();
                            $mas[] = array('answer' => $answer);
                            continue;
                        }
                    } else {
                        $answer = 'Категория с таким idNumber не найдена';
                        $mas[] = array('answer' => $answer);
                        continue;
                    }
                }
            }

            $course = (array)$course;
            if ($course['id'] && $users) {
                foreach ($users as $username)
                    try {
                        self::enrol_to_course($username, $course['id']);
                    } catch (Exception $ex) {
                        $answer = $ex->getMessage();
                        $mas[] = array('answer' => $answer);
                        continue;
                    }
            }
            $mas[] = array('answer' => $answer);
            $log .= "|$num";
            $num++;
        }

        if ($delete_course) {
            $delete_course_keys = array_reverse($delete_course_keys);
            foreach ($delete_course_keys as $dk) {
                $course = $delete_course[$dk];

                if ($c = $DB->get_record_sql("select * from {course} where id = '{$course['id']}' or idnumber = '{$course['idNumber']}'")) {
                    delete_course($c, false);
                    $mas[$dk] = array('answer' => 'Курс удален');
                } else $mas[$dk] = array('answer' => 'Курс не найден');
            }
        }
        if ($delete_category) {
            $delete_category_keys = array_reverse($delete_category_keys);
            foreach ($delete_category_keys as $dk) {
                $course = $delete_category[$dk];

                if ($c = $DB->get_record_sql("select * from {course_categories} where id = '{$course['id']}' or idnumber = '{$course['idNumber']}'")) {

                    if ($DB->record_exists('course', array('category' => $c->id)))
                        $mas[$dk] = array('answer' => 'В данной категории имеются курсы');
                    elseif ($DB->record_exists('course_categories', array('parent' => $c->id)))
                        $mas[$dk] = array('answer' => 'В данной категории имеются другие категории');
                    else {
                        $cat = coursecat::get($c->id);
                        $cat->delete_full(false);
                        $mas[$dk] = array('answer' => 'Категория удалена');
                    }
                } else $mas[$dk] = array('answer' => 'Категория не найдена');
            }
        }
        return $mas;
    }

    public static function get_courses_info()
    {
        global $DB, $CFG;

        /*$course = new stdClass();

        $course->id = 1;
        $course->idNumber = "idnumber";
        $course->name = "123";
        $course->parentCategory = 1;
        $course->isCategory = false;*/

        $result = array();

        if ($courses = $DB->get_records('course')) {
            foreach ($courses as $c) {
                $course = new stdClass();
                $course->id = $c->id;
                $course->idNumber = $c->idnumber;
                $course->name = $c->fullname;
                $course->parentCategory = $c->category;
                $course->isCategory = false;
                $course->users = array();
                if ($users = self::get_contacts_by_courses($c)) {
                    foreach ($users as $u) $course->users[] = $u->username;
                }

                $result[] = (array)$course;
            }


        }

        if ($courses = $DB->get_records('course_categories')) {
            foreach ($courses as $c) {
                $course = new stdClass();
                $course->id = $c->id;
                $course->idNumber = $c->idnumber;
                $course->name = $c->name;
                $course->parentCategory = $c->parent;
                $course->isCategory = true;
                $course->users = array();
                $result[] = (array)$course;
            }


        }


        return $result;
    }

    /**
     * Returns description of get_instance_info() result value.
     *
     * @return external_description
     */
    public static function set_courses_info_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'answer' => new external_value(PARAM_TEXT, 'answer')
                )
            )
        );
    }

    public static function get_grades_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'id' => new external_value(PARAM_TEXT, 'user AD'),
                    'grade' => new external_value(PARAM_FLOAT, 'grade'),
                    'idNumber' => new external_value(PARAM_TEXT, 'id of course/category in 1C'),
                    'date' => new external_value(PARAM_INT, 'date to UNIXTIME')
                )
            )
        );
    }

    public static function set_enrol_user_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'answer' => new external_value(PARAM_TEXT, 'answer'),
                    'users' => new external_multiple_structure(
                        new external_value(PARAM_TEXT, 'answer for username')
                    )
                )
            )
        );
    }

    public static function get_courses_info_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'id' => new external_value(PARAM_INT, 'id of course/category in Moodle'),
                    'idNumber' => new external_value(PARAM_TEXT, 'id of course/category in 1C'),
                    'name' => new external_value(PARAM_TEXT, 'course/category name'),
                    'parentCategory' => new external_value(PARAM_INT, 'name of enrolment plugin'),
                    'isCategory' => new external_value(PARAM_BOOL, 'status of enrolment plugin'),
                    'users' => new external_multiple_structure(
                        new external_value(PARAM_TEXT, 'username')
                    )
                )
            )
        );
    }
}
