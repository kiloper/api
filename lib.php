<?php


function local_api_cron()
{
    global $DB;
    $now  = time();
    if($result = $DB->get_records_sql('select * from {api_enrols} where opened = 1 and timestart < '.$now))
    {
        foreach($result as $r)
        {
            if($DB->record_exists('api_enrols_db', array('username' => $r->username, 'courseid' => $r->courseid))) continue;
            $ob = new stdClass();
            $ob->username = $r->username;
            $ob->courseid = $r->courseid;
            $DB->insert_record('api_enrols_db', $ob);
        }

    }
    if($enrols = $DB->get_records_sql('select * from {api_enrols} where opened = 1 and timestart + 3600*24 < '.$now))
    {
        foreach($enrols as $enrol)
        {
            $enrol->opened = 0;
            $DB->update_record('api_enrols', $enrol);
            $DB->delete_records('api_enrols_db', array('username' => $enrol->username, 'courseid' => $enrol->courseid));
            /*if($user = $DB->get_record('user', array('username' => $enrol->username))) {
                if ($attempt = $DB->get_record_sql("
                  select c.id, a.userid
                  from
                  {quiz_attempts} a left join {quiz} q on a.quiz = q.id
                  left join {course} c on q.course = c.id
                  where
                    a.userid = {$user->id}
                    and c.id = {$enrol->courseid}
                    and a.timefinish > {$enrol->timestart}")
                ) {
                    $enrol->opened = 0;
                    $DB->update_record('api_enrols', $enrol);
                    $DB->delete_records('api_enrols_db', array('username' => $enrol->username, 'courseid' => $enrol->courseid));
                }
            }*/
        }
    }
    return true;
}

?>