<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Self enrol plugin external functions and service definitions.
 *
 * @package   enrol_self
 * @copyright 2013 Rajesh Taneja <rajesh@moodle.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since     Moodle 2.6
 */

$functions = array(
    'getCoursesStructure' => array(
        'classname'   => 'local_api_external',
        'methodname'  => 'get_courses_info',
        'classpath'   => 'local/api/externallib.php',
        'description' => 'local api information for get courses',
        'type'        => 'read'
    ),
    'setCoursesStructure' => array(
        'classname'   => 'local_api_external',
        'methodname'  => 'set_courses_info',
        'classpath'   => 'local/api/externallib.php',
        'description' => 'local api information for set courses',
        'type'        => 'write'
    ),
    'setEnrolUser' => array(
        'classname'   => 'local_api_external',
        'methodname'  => 'set_enrol_user',
        'classpath'   => 'local/api/externallib.php',
        'description' => 'local api information for set enrol',
        'type'        => 'write'
    ),
    'getGrades' => array(
        'classname'   => 'local_api_external',
        'methodname'  => 'get_grades',
        'classpath'   => 'local/api/externallib.php',
        'description' => 'local api information for set enrol',
        'type'        => 'read'
    )
);
